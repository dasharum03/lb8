#include "pch.h"
#include "CppUnitTest.h"
#include "../main-project/wind_rose.h"
#include "../main-project/processing.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace Test
{
	TEST_CLASS(Test)
	{
	public:
		TEST_METHOD(TestMethod1)
		{
			wind_rose* wind = new wind_rose[1];
			wind[0].speed = 10;
			wind[0].dt.month = 3;

			bool flag;
			if (10 == Summ_Speed_by_month(&wind, 1, 3)) flag = true;
			Assert::IsTrue(flag);
		}
		TEST_METHOD(TestMethod2)
		{
			wind_rose* wind = new wind_rose[1];
			wind[0].speed = 20;
			wind[0].dt.month = 4;

			bool flag;
			if (20 == Summ_Speed_by_month(&wind, 1, 4)) flag = true;
			Assert::IsTrue(flag);
		}
		TEST_METHOD(TestMethod3)
		{
			wind_rose* wind = new wind_rose[1];
			wind[0].speed = 1.1;
			wind[0].dt.month = 3;

			bool flag;
			if (1.1 == Summ_Speed_by_month(&wind, 1, 3)) flag = true;
			Assert::IsTrue(flag);
		}
	};
}
