#pragma once
#ifndef INFORMATION_H
#define INFORMATION_H

#include "constants.h"

struct date
{
	unsigned day;
	unsigned month;
};

struct wind_rose
{
	date    dt;
	char    direction[MAX_STRING_SIZE];
	double  speed;
};


#endif