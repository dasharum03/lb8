#include <iostream>
#include "file_reader.h"
#include "output.h"
#include "bydirection.h"
#include "byspeed.h"
#include "processing.h"

using namespace std;

int main()
{
	setlocale(LC_ALL, "Russian");
	cout << "Лабораторная работа №8. GIT\n";
	cout << "Вариант №6. Роза ветров\n";
	cout << "Автор: Дарья Рум\n";
	cout << "Группа: 16\n";
	wind_rose* windrose[MAX_FILE_ROWS_COUNT];
	int size;
	try
	{
		read("data.txt", windrose, size);

		cout << "Весь список розы ветров" << "\n";
		Print_Wind_All(windrose, size);
		cout << "Cписок розы ветров по направлению West" << "\n";
		Print_Wind_By_Direction(windrose, size, "West");
		cout << "Cписок розы ветров по направлению NorthWest" << "\n";
		Print_Wind_By_Direction(windrose, size, "NorthWest");
		cout << "Cписок розы ветров по направлению North" << "\n";
		Print_Wind_By_Direction(windrose, size, "North");
		cout << "Cписок розы ветров по скорости > 5 м/c" << "\n";
		Print_Wind_By_Speed(windrose, size, 5.0);
		
		cout << "Введите месяц" << "\n";
		unsigned m;
		cin >> m;
		double sm = Summ_Speed_by_month(windrose, size, m);
		cout << "Сумма =" << sm << "\n";

		for (int i = 0; i < size; i++)
		{
			delete windrose[i];
		}
	}
	catch (const char* error)
	{
		cout << error << '\n';
	}
	getchar();
	return 0;

}
