#include "processing.h"
double Summ_Speed_by_month(wind_rose* windrose[MAX_FILE_ROWS_COUNT], int size, unsigned month)
{
	double sum = 0.0;
	for (int i = 0; i < size; i++)
	{
		if (windrose[i]->dt.month == month) {
			sum += windrose[i]->speed;
		}
	}
	return sum;
}