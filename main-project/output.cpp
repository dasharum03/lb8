#include "output.h"
void print_item_wind(wind_rose* windrose)
{
	cout << windrose->dt.day << '.';
	cout << windrose->dt.month << ' ';
	cout << windrose->direction << ' ';
	cout << windrose->speed << '\n';
}

void Print_Wind_All(wind_rose* windrose[MAX_FILE_ROWS_COUNT], int size)
{
	for (int i = 0; i < size; i++)
	{
		print_item_wind(windrose[i]);
	}
}