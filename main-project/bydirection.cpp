#include "bydirection.h"
void Print_Wind_By_Direction(wind_rose* windrose[MAX_FILE_ROWS_COUNT], int size, const char* direct)
{
	for (int i = 0; i < size; i++)
	{
		if (strcmp(windrose[i]->direction, direct) == 0) {
			cout << windrose[i]->dt.day << '.';
			cout << windrose[i]->dt.month << '\n';
		}
	}
}