#include "byspeed.h"
void Print_Wind_By_Speed(wind_rose* windrose[MAX_FILE_ROWS_COUNT], int size, double speed)
{
	for (int i = 0; i < size; i++)
	{
		if (windrose[i]->speed > speed) {
			cout << windrose[i]->dt.day << '.';
			cout << windrose[i]->dt.month << '\n';
		}
	}
}